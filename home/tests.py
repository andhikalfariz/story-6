from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Kegiatan, Anggota


# Create your tests here.

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.kegiatan_url = reverse('home:kegiatan')
        self.test_kegiatan = Kegiatan.objects.create(
            kegiatan = 'makan'
        )
        self.test_anggota = Anggota.objects.create(
            anggota = 'Andhika',
            aktivitas = self.test_kegiatan
        )
        self.test_delete_url = reverse("home:kegiatanDelete", args=[self.test_kegiatan.id])
        self.test_anggota_delete_url = reverse("home:anggotaDelete", args=[self.test_anggota.id])

        def test_kegiatan_GET(self):
            response = self.client.get(self.kegiatan_url)
            self.assertEquals(response.status_code, 200)
            self.assertTemplateUsed(response, "home/index.html")
        
        def test_kegiatan_POST(self):
            response = self.client.post(self.kegiatan_url, {
                "kegiatan" : "tidur"
            }, follow=True)
            self.assertContains(response, "tidur")

        def test_anggota_POST(self):
            response = self.client.post(self.kegiatan_url, {
                "anggota" : "Hadi",
                "aktivitas" : "self.test_kegiatan.id",
            }, follow=True)
            self.assertContains(response, "Hadi")

        def test_notValid_POST(self):
            response = self.client.post(self.kegiatan_url, {
            "anggota" : "Hadi",
            "aktivitas" : "haha",
            }, follow=True)
            self.assertContains(response, "Input tidak sesuai")

        def test_anggota_DELETE(self):
            response = self.client.get(self.test_anggota_delete_url, follow=True)
            print(response.content)
            self.assertContains(response, "berhasil dihapus")

        def test_kegiatan_DELETE(self):
            response = self.client.get(self.test_delete_url, follow=True)
            self.assertContains(response, "berhasil dihapus")