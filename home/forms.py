from django import forms
from .models import Kegiatan, Anggota

class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'
        widgets = {
            'kegiatan' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Apa yang ingin anda lakukan?"
            }),
        }

class FormAnggota(forms.ModelForm):
    class Meta:
        model = Anggota
        fields = '__all__' 
        widgets = {
            'anggota' : forms.TextInput(attrs= {
                'class': 'form-control',
                'placeholder': "Ingin bergabung?"
            }),
        }