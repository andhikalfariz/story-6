from django.contrib import admin
from .models import Kegiatan, Anggota

# Register your models here.

admin.site.register(Kegiatan)
admin.site.register(Anggota)