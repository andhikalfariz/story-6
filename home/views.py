from django.shortcuts import render, redirect
from .models import Anggota, Kegiatan
from .forms import FormAnggota, FormKegiatan
from django.contrib import messages


# Create your views here.

def kegiatan(request):
    if request.method == "POST":
        formA = FormKegiatan(request.POST)
        formB = FormAnggota(request.POST)
        
        if formA.is_valid():
            formA.save()
            messages.success(request, (f"Kegiatan {request.POST['kegiatan']} berhasil ditambahkan!"))
            return redirect('home:kegiatan')

        elif formB.is_valid():
            formB.save()
            messages.success(request, (f"{request.POST['anggota']} berhasil bergabung!"))
            return redirect('home:kegiatan')

        else :
            messages.warning(request, (f"Input tidak sesuai!"))
            return redirect('home:kegiatan')
    
    else:
        formA = FormKegiatan()
        formB = FormAnggota()
        listKegiatan = Kegiatan.objects.all()
        listAnggota = Anggota.objects.all()
        context = {
            'formA' : formA,
            'formB' : formB,
            'listKegiatan' : listKegiatan,
            'listAnggota' : listAnggota,
        }
        return render(request,'home/index.html', context)

def delete(request,pk):
    kegiatan = Kegiatan.objects.get(id=pk)
    kegiatan.delete()
    messages.warning(request, (f"Kegiatan {kegiatan} berhasil dihapus!"))
    return redirect('home:kegiatan')

def deleteAnggota(request,pk):
    anggota = Anggota.objects.get(id=pk)
    anggota.delete()
    messages.warning(request, (f"{anggota} berhasil dihapus"))
    return redirect('home:kegiatan')
