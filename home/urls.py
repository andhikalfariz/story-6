from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('delete/<str:pk>', views.delete, name='kegiatanDelete'),
    path('delete/anggota/<str:pk>', views.deleteAnggota, name='anggotaDelete'),
]